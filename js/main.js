;(function ($) {
  "use strict";

  /*----------------------------
    Fence Repair Navigation
  ------------------------------ */
  $('.navbar-toggler').on('click', function(){
    $(this).toggleClass('is-active');
    $('.navbar-collapse').toggleClass('is-active');
  });

  /*----------------------------
    Fence Repair Testimonal Slider
  ------------------------------ */
  $('.fr-testimonial__info').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    swipeToSlide: true,
    pauseOnHover: false,
    prevArrow: '<button class="slick-prev slick-arrow"></button>',
    nextArrow: '<button class="slick-next slick-arrow"></button>'
  });

})(jQuery);
