<?php
function fence_hero_section()
{
    vc_map(
        array(
            'name'      => __( 'Hero section', 'fencerepair' ),
            'base'      => 'code_hero',
            'category'  => __( 'Fencerepair', 'fencerepair' ),
            'params'    => array(
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'heading'       => __( 'Title', 'fencerepair' ),
                    'description'   => __( 'HTML tags like span or br can be used in this field.' ),
                    'param_name'    => 'hero_title',
                    'save_always'   => true,
                ),
                array(
                    'type'          => 'textarea',
                    'holder'        => 'div',
                    'heading'       => __( 'Summary', 'fencerepair' ),
                    'description'   => __( 'HTML tags like span or br can be used in this field.' ),
                    'param_name'    => 'hero_summary',
                    'save_always'   => true,
                ),
                array(
                    'type'          => 'textarea_html',
                    'holder'        => 'div',
                    'heading'       => __( 'Add shortcode/embedable code', 'fencerepair' ),
                    'description'   => __( 'Enter a shortcode or any html embedable code here.' ),
                    'param_name'    => 'content',
                    'save_always'   => true,
                ),
                array(
                    'type'          => 'attach_image',
                    'heading'       => __( 'Upload background image', 'fencerepair' ),
                    'description'   => __( 'Upload a background image for hero section.' ),
                    'param_name'    => 'hero_bg_image',
                    'save_always'   => true
                ),
                array(
                    'type'          => 'attach_images',
                    'heading'       => __( 'Upload image', 'fencerepair' ),
                    'description'   => __( 'Multiple images can be added here for hero section.' ),
                    'param_name'    => 'hero_images',
                    'save_always'   => true
                ),
            )
        )
    );
}

add_action( 'vc_before_init', 'fence_hero_section' );

// Output
function fence_hero_output( $atts, $content )
{

    extract(shortcode_atts(array(
        'hero_title'     => '',
        'hero_summary'   => '',
        'content'        => $content,
        'hero_bg_image'  => '',
        'hero_images'    => '',
    ), $atts));

    ob_start();
?>
    <section class="fr-hero">
        <div class="fr-container">
            <div class="fr-hero__bg-image" style="background-image:url(<?php echo wp_get_attachment_image_url( $hero_bg_image, 'full' ); ?>)"></div>
            <h1><?php echo apply_filters('the_title', $hero_title); ?></h1>
            <p><?php echo apply_filters('hero_summary', $hero_summary); ?></p>
            <div class="fr-hero__footer">
                <div class="fr-hero__form">
                    <?php echo apply_filters('the_content', $content); ?>
                </div>
                <?php if( ! empty( $hero_images ) ) : ?>
                <div class="fr-hero__thumbs">
                <?php
                    $hero_image = explode(',', $hero_images);
                    foreach( $hero_image as $hero_img ) :
                ?>
                    <img src="<?php esc_attr_e(wp_get_attachment_image_url( $hero_img, 'full' ) ); ?>" alt="<?php esc_attr_e(get_post_meta( $hero_img, '_wp_attachment_image_alt', true )); ?>">
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php
    return ob_get_clean();
}

add_shortcode( 'code_hero', 'fence_hero_output' );
