<?php
function fence_menu_section()
{
    // Retrieve menu locations
    $main_menu_locations = array_merge(['None' => false], array_flip(get_registered_nav_menus()));

    vc_map(
        array(
            'name'      => __('Menu', 'fencerepair'),
            'base'      => 'code_menu',
            'category'  => __('Fencerepair', 'fencerepair'),
            'params'    => array(
                array(
                    'type'          => 'attach_image',
                    'holder'        => 'img',
                    'heading'       => __('Upload a logo', 'fencerepair'),
                    'param_name'    => 'logo',
                    'save_always'   => true
                ),
                array(
                    'type'          => 'dropdown',
                    'heading'       => __('Select a menu location', 'fencerepair'),
                    'value'         => $main_menu_locations,
                    'description'   => __('Select a menu location which will be added in the main menu.', 'fencerepair'),
                    'param_name'    => 'main_menu_item'
                ),
            )
        )
    );
}

add_action('vc_before_init', 'fence_menu_section');

// Output
function fence_menu_output($atts, $content)
{
    extract(shortcode_atts(array(
        'logo'             => '',
        'main_menu_item'   => 'none',
    ), $atts));

    ob_start();
?>
    <header class="fr-header">
        <div class="fr-container">
            <nav class="navbar">
                <?php if (!empty($logo)) : ?>
                    <a href="<?php echo esc_url(site_url('/')); ?>">
                        <img src="<?php esc_attr_e(wp_get_attachment_image_url($logo, 'full')); ?>" alt="<?php esc_attr_e(get_post_meta($hero_img, '_wp_attachment_image_alt', true)); ?>">
                    </a>
                <?php
                elseif (has_custom_logo()) :
                    the_custom_logo();
                else :
                ?>
                    <a href="<?php echo esc_url(site_url('/')); ?>">
                        <img src="<?php echo plugins_url('fencerepairs/images/FenceRepairsLogo.svg'); ?>" alt="FenceRepair Logo">
                    </a>
                <?php endif; ?>
                <div class="navbar-toggler">
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-text"><?php esc_html_e('Menu'); ?></span>
                </div>
                <div class="navbar-collapse">
                <?php
                    if( $main_menu_item != false ) {
                        wp_nav_menu([
                            'theme_location'    => $main_menu_item,
                            'menu_class'        => 'fr-navbar'
                        ]);
                    }
                ?>
                </div>
            </nav>
        </div>
    </header>
<?php
    return ob_get_clean();
}

add_shortcode('code_menu', 'fence_menu_output');
