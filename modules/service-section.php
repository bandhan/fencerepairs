<?php
function fence_service_section()
{
    vc_map(
        array(
            'name'      => __( 'Service section', 'fencerepair' ),
            'base'      => 'code_service',
            'category'  => __( 'Fencerepair', 'fencerepair' ),
            'params'    => array(
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'heading'       => __( 'Title', 'fencerepair' ),
                    'param_name'    => 'service_title',
                    'save_always'   => true,
                ),
                array(
                    'type'          => 'textarea',
                    'holder'        => 'div',
                    'heading'       => __( 'Summary', 'fencerepair' ),
                    'param_name'    => 'service_summary',
                    'save_always'   => true,
                ),
                array(
                    'type'          => 'param_group',
                    'heading'       => 'Add new service',
                    'param_name'    => 'services_group',
                    'params' => array(
                        array(
                            'type'          => 'attach_image',
                            'heading'       => __( 'Upload background image', 'fencerepair' ),
                            'param_name'    => 'service_img',
                            'save_always'   => true
                        ),
                        array(
                            'type'          => 'textfield',
                            'heading'       => __( 'Title', 'fencerepair' ),
                            'param_name'    => 'title',
                            'save_always'   => true,
                        ),
                        array(
                            'type'          => 'textarea',
                            'heading'       => __( 'Description', 'fencerepair' ),
                            'param_name'    => 'service_desc',
                            'save_always'   => true,
                        ),
                    )
                ),
            )
        )
    );
}

add_action( 'vc_before_init', 'fence_service_section' );

// Output
function fence_service_output( $atts, $content )
{

    extract(shortcode_atts(array(
        'service_title'     => '',
        'service_summary'   => '',
        'services_group'     => '',
    ), $atts));

    $service_groups = vc_param_group_parse_atts( $services_group );

    ob_start();
?>
    <section class="fr-services">
        <div class="fr-container">
            <div class="fr-services__heading">
                <h2><?php esc_html_e( $service_title ); ?></h2>
                <p><?php esc_html_e( $service_summary ); ?></p>
            </div>
            <div class="fr-services__info">
                <?php
                    if( ! empty( $service_groups ) ) :
                        foreach( $service_groups as $service_group ) :
                ?>
                <div class="fr-services__single">
                    <?php if( ! empty( $service_group['service_img'] ) ) : ?>
                    <img src="<?php esc_attr_e( wp_get_attachment_image_url( $service_group['service_img'], 'full' ) ); ?>"
                        alt="<?php esc_attr_e(get_post_meta( $service_group['service_img'], '_wp_attachment_image_alt', true )); ?>">
                    <?php endif; ?>
                    <h3><?php esc_html_e( $service_group['title'] ); ?></h3>
                    <?php echo wpautop(apply_filters( 'the_service_content', $service_group['service_desc'] )); ?>
                </div>
                <?php
                        endforeach;
                    endif;
                ?>
            </div>
        </div>
    </section>
<?php
    return ob_get_clean();
}

add_shortcode( 'code_service', 'fence_service_output' );
