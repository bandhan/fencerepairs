<?php
function fence_contact_section()
{
    vc_map(
        array(
            'name'      => __( 'contact', 'fencerepair' ),
            'base'      => 'code_contact',
            'category'  => __( 'Fencerepair', 'fencerepair' ),
            'params'    => array(
                array(
                    'type'          => 'attach_image',
                    'holder'        => 'img',
                    'heading'       => __( 'Upload a background image', 'fencerepair' ),
                    'param_name'    => 'contact_bg_img',
                    'save_always'   => true
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'heading'       => __( 'Title', 'fencerepair' ),
                    'param_name'    => 'contact_title',
                    'save_always'   => true,
                ),
                array(
                    'type'          => 'textarea',
                    'holder'        => 'div',
                    'heading'       => __( 'Summary', 'fencerepair' ),
                    'param_name'    => 'contact_desc',
                    'save_always'   => true,
                ),
                array(
                    'type'          => 'attach_image',
                    'holder'        => 'img',
                    'heading'       => __( 'Upload an image', 'fencerepair' ),
                    'param_name'    => 'contact_img',
                    'save_always'   => true
                ),
                array(
                    'type'          => 'textarea_html',
                    'holder'        => 'div',
                    'heading'       => __( 'Add shortcode/embedable code', 'fencerepair' ),
                    'param_name'    => 'content',
                    'save_always'   => true,
                ),
            ),
        ),
    );
}

add_action( 'vc_before_init', 'fence_contact_section' );

// Output
function fence_contact_output( $atts, $content )
{

    extract(shortcode_atts(array(
        'contact_bg_img'  => '',
        'contact_title'   => '',
        'contact_desc'    => '',
        'contact_img'     => '',
        'content'         => $content,
    ), $atts));

    ob_start();
?>
    <section class="fr-contact" style="background-image:url(<?php esc_attr_e( wp_get_attachment_image_url( $contact_bg_img, 'full' )); ?>)">
        <div class="fr-container">
            <div class="fr-contact__info">
                <div class="fr-contact__content">
                    <h2><?php esc_html_e( $contact_title ); ?></h2>
                    <p><?php esc_html_e( $contact_desc ); ?></p>
                    <img src="<?php esc_attr_e( wp_get_attachment_image_url( $contact_img, 'full' ) ); ?>"
                        alt="<?php esc_attr_e(get_post_meta( $contact_img, '_wp_attachment_image_alt', true )); ?>">
                </div>
                <?php if( ! empty( $content ) ) : ?>
                <div class="fr-contact__form">
                    <div class="fr-contact__form-block">
                        <?php echo apply_filters('the_content', $content); ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php
    return ob_get_clean();
}

add_shortcode( 'code_contact', 'fence_contact_output' );
