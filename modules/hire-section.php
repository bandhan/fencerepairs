<?php
function fence_hire_section()
{
    vc_map(
        array(
            'name'      => __( 'Hire section', 'fencerepair' ),
            'base'      => 'code_hire',
            'category'  => __( 'Fencerepair', 'fencerepair' ),
            'params'    => array(
                array(
                    'type'          => 'attach_image',
                    'holder'        => 'img',
                    'heading'       => __( 'Upload background image', 'fencerepair' ),
                    'param_name'    => 'hire_bg_image',
                    'save_always'   => true
                ),
                array(
                    'type'          => 'textarea_html',
                    'holder'        => 'div',
                    'heading'       => __( 'Contents', 'fencerepair' ),
                    'param_name'    => 'content',
                    'save_always'   => true,
                ),
            )
        )
    );
}

add_action( 'vc_before_init', 'fence_hire_section' );

// Output
function fence_hire_output( $atts, $content )
{

    extract(shortcode_atts(array(
        'content'        => $content,
        'hire_bg_image'  => '',
    ), $atts));

    ob_start();
?>
    <section class="fr-hire">
        <?php if( ! empty( $hire_bg_image ) ) : ?>
        <div class="fr-hire__left">
            <img src="<?php esc_attr_e( wp_get_attachment_image_url( $hire_bg_image, 'full' ) ); ?>"
                alt="<?php esc_attr_e(get_post_meta( $hire_bg_image, '_wp_attachment_image_alt', true )); ?>">
        </div>
        <?php endif; ?>
        <?php if( ! empty( $content ) ) : ?>
        <div class="fr-hire__content">
            <?php echo wpautop( apply_filters( 'the_hire_content', $content ) ); ?>
        </div>
        <?php endif; ?>
  </section>
<?php
    return ob_get_clean();
}

add_shortcode( 'code_hire', 'fence_hire_output' );
