<?php
function fence_footer_section()
{
    // Retrieve menu locations
    $menu_locations = array_merge(['None' => false], array_flip(get_registered_nav_menus()));

    vc_map(
        array(
            'name'      => __( 'Footer', 'fencerepair' ),
            'base'      => 'code_footer',
            'category'  => __( 'Fencerepair', 'fencerepair' ),
            'params'    => array(
                array(
                    'type'          => 'textarea',
                    'holder'        => 'div',
                    'heading'       => __( 'Enter footer copyright text', 'fencerepair' ),
                    'param_name'    => 'footer_copyright',
                    'save_always'   => true,
                ),
                array(
                    'type'          => 'dropdown',
                    'heading'       => __( 'Select a menu location', 'fencerepair' ),
                    'value'         => $menu_locations,
                    'description'   => __( 'Select a menu location which will be added in the footer.', 'fencerepair' ),
                    'param_name'    => 'menu_item'
                ),
            )
        )
    );
}

add_action( 'vc_before_init', 'fence_footer_section' );

// Output
function fence_footer_output( $atts, $content )
{
    extract(shortcode_atts(array(
        'footer_copyright'  => '',
        'menu_item'         => 'none',
    ), $atts));

    ob_start();
?>
    <footer class="fr-footer fr-footer__two">
        <div class="fr-container">
            <p><?php echo apply_filters('footer_copyright', $footer_copyright); ?></p>
            <div class="fr-footer__nav">
            <?php
                if($menu_item != false) {
                    wp_nav_menu([
                        'theme_location'    => $menu_item,
                        'menu_class'        => 'fr-footer__navbar'
                    ]);
                }
            ?>
            </div>
        </div>
    </footer>
<?php
    return ob_get_clean();
}

add_shortcode( 'code_footer', 'fence_footer_output' );
