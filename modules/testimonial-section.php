<?php
function fence_testimonial_section()
{
    vc_map(
        array(
            'name'      => __( 'Testimonial', 'fencerepair' ),
            'base'      => 'code_testimonial',
            'category'  => __( 'Fencerepair', 'fencerepair' ),
            'params'    => array(
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'heading'       => __( 'Title', 'fencerepair' ),
                    'param_name'    => 'testi_title',
                    'save_always'   => true,
                ),
                array(
                    'type'          => 'param_group',
                    'heading'       => 'Add new testimonial',
                    'param_name'    => 'testi_group',
                    'params' => array(
                        array(
                            'type'          => 'textarea',
                            'heading'       => __( 'Testimonial', 'fencerepair' ),
                            'param_name'    => 'testi_desc',
                            'save_always'   => true,
                        ),
                        array(
                            'type'          => 'textfield',
                            'heading'       => __( 'Commentor', 'fencerepair' ),
                            'param_name'    => 'testi_commentor',
                            'save_always'   => true,
                        ),
                    )
                ),
            )
        )
    );
}

add_action( 'vc_before_init', 'fence_testimonial_section' );

// Output
function fence_testimonial_output( $atts, $content )
{

    extract(shortcode_atts(array(
        'testi_title'   => '',
        'testi_group'   => '',
    ), $atts));

    $testi_groups = vc_param_group_parse_atts( $testi_group );

    ob_start();
?>
    <section class="fr-testimonial">
        <div class="fr-container">
            <h2><?php esc_html_e( $testi_title ); ?></h2>
            <div class="fr-testimonial__info">
                <?php
                    if( ! empty( $testi_groups ) ) :
                        foreach ( $testi_groups as $teste_group ) :
                ?>
                <div class="fr-testimonial__single">
                    <p><?php esc_html_e( $teste_group['testi_desc'] ); ?></p>
                    <span><?php esc_html_e( $teste_group['testi_commentor'] ); ?></span>
                </div>
                <?php
                        endforeach;
                    endif;
                ?>
            </div>
        </div>
    </section>
<?php
    return ob_get_clean();
}

add_shortcode( 'code_testimonial', 'fence_testimonial_output' );
