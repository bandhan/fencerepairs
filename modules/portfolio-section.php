<?php
function fence_portfolio_section()
{
    vc_map(
        array(
            'name'      => __( 'Portfolio', 'fencerepair' ),
            'base'      => 'code_portfolio',
            'category'  => __( 'Fencerepair', 'fencerepair' ),
            'params'    => array(
                array(
                    "type" => "checkbox",
                    "heading" => __( "Equal sized row", "fencerepair" ),
                    "param_name" => "equal_row",
                    "description" => __( "Make all rows equal in size and arrangment.", "fencerepair" )
                ),
                array(
                    'type'          => 'attach_image',
                    'holder'        => 'img',
                    'heading'       => __( 'Upload background image', 'fencerepair' ),
                    'param_name'    => 'portfolio_bg_img',
                    'save_always'   => true
                ),
                array(
                    'type'          => 'textarea_html',
                    'holder'        => 'div',
                    'heading'       => __( 'Contents', 'fencerepair' ),
                    'param_name'    => 'content',
                    'save_always'   => true,
                ),
                array(
                    'type'          => 'attach_images',
                    'heading'       => __( 'Upload portfolios', 'fencerepair' ),
                    'param_name'    => 'portfolio_imgs',
                    'save_always'   => true
                ),
            ),
        ),
    );
}

add_action( 'vc_before_init', 'fence_portfolio_section' );

// Output
function fence_portfolio_output( $atts, $content )
{
    extract(shortcode_atts(array(
        'equal_row'         => '',
        'portfolio_bg_img'  => '',
        'content'           => $content,
        'portfolio_imgs'    => '',
    ), $atts));

    ob_start();
?>
    <section class="fr-portfolio<?php echo ( $equal_row == true ) ? ' fr-portfolio__grid' : ''; ?>">
        <div class="fr-portfolio__icon-top" style="background-image:url(<?php esc_attr_e( wp_get_attachment_image_url( $portfolio_bg_img, 'full' ) ); ?>)"></div>
        <div class="fr-portfolio__icon-bottom" style="background-image:url(<?php esc_attr_e( wp_get_attachment_image_url( $portfolio_bg_img, 'full' ) ); ?>)"></div>
        <div class="fr-container">
            <div class="fr-portfolio__content">
                <?php echo wpautop(apply_filters('portfolio_content', $content)); ?>
            </div>
        </div>
        <?php if( ! empty( $portfolio_imgs ) ) : ?>
        <div class="fr-portfolio__gallery">
            <?php
            $portfolio_lists = explode(',', $portfolio_imgs);
            foreach( $portfolio_lists as $portfolio_list ) :
            ?>
            <div class="fr-portfolio__gallery-single">
                <img src="<?php esc_attr_e( wp_get_attachment_image_url( $portfolio_list, 'full' ) ); ?>"
                    alt="<?php esc_attr_e(get_post_meta( $portfolio_list, '_wp_attachment_image_alt', true )); ?>">
            </div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>
    </section>
<?php
    return ob_get_clean();
}

add_shortcode( 'code_portfolio', 'fence_portfolio_output' );
