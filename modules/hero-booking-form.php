<?php
function fence_booking_form_section()
{
    vc_map(
        array(
            'name'      => __( 'Hero + Booking Form', 'fencerepair' ),
            'base'      => 'code_booking_form',
            'category'  => __( 'Fencerepair', 'fencerepair' ),
            'params'    => array(
                array(
                    'type'          => 'attach_image',
                    'holder'        => 'img',
                    'heading'       => __( 'Upload a background image', 'fencerepair' ),
                    'param_name'    => 'booking_bg',
                    'save_always'   => true
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'heading'       => __( 'Booking title', 'fencerepair' ),
                    'param_name'    => 'booking_title',
                    'save_always'   => true,
                ),
                array(
                    'type'          => 'textarea',
                    'holder'        => 'div',
                    'heading'       => __( 'Booking Content', 'fencerepair' ),
                    'param_name'    => 'booking_content',
                    'save_always'   => true,
                ),
                array(
                    'type'          => 'checkbox',
                    'heading'       => __( 'Need CTA?', 'fencerepair' ),
                    'param_name'    => 'cta_activate',
                    'save_always'   => true,
                ),
                array(
                    'type'          => 'textfield',
                    'heading'       => __( 'Button name', 'fencerepair' ),
                    'param_name'    => 'btn_name',
                    'save_always'   => true,
                    'dependency'    => array(
                        'element'   => 'cta_activate',
                        'value'     => 'true'
                    )
                ),
                array(
                    'type'          => 'textfield',
                    'heading'       => __( 'Button URL', 'fencerepair' ),
                    'param_name'    => 'btn_url',
                    'save_always'   => true,
                    'dependency'    => array(
                        'element'   => 'cta_activate',
                        'value'     => 'true'
                    )
                ),
                array(
                    'type'          => 'textfield',
                    'heading'       => __( 'Location title', 'fencerepair' ),
                    'param_name'    => 'location_title',
                    'save_always'   => true,
                ),
                array(
                    'type'          => 'textfield',
                    'heading'       => __( 'Form title', 'fencerepair' ),
                    'param_name'    => 'form_title',
                    'save_always'   => true,
                ),
                array(
                    'type'          => 'textarea_html',
                    'heading'       => __( 'Add shortcode/embedable code', 'fencerepair' ),
                    'param_name'    => 'content',
                    'save_always'   => true,
                ),
            ),
        ),
    );
}

add_action( 'vc_before_init', 'fence_booking_form_section' );

// Output
function fence_booking_form_output( $atts, $content )
{

    extract(shortcode_atts(array(
        'booking_title'    => '',
        'booking_content'  => '',
        'cta_activate'     => '',
        'btn_name'         => '',
        'btn_url'          => '',
        'booking_bg'       => '',
        'content'          => $content,
        'location_title'   => '',
        'form_title'       => '',
    ), $atts));

    ob_start();
?>
    <section class="fr-welcome" style="background-image:url(<?php esc_attr_e( wp_get_attachment_image_url( $booking_bg, 'full' ) ); ?>)">
        <div class="fr-container">
            <div class="fr-welcome__info">
                <div class="fr-welcome__content">
                    <h1><?php esc_html_e( $booking_title ); ?></h1>
                    <?php echo wpautop(apply_filters('booking_content', $booking_content)); ?>
                    <?php if(true == $cta_activate) : ?>
                    <a href="<?php esc_attr_e( $btn_url ); ?>"><?php esc_html_e( $btn_name ); ?></a>
                    <?php endif; ?>
                </div>
                <?php if( ! empty( $location_title || $form_title || $content ) ) : ?>
                <div class="fr-welcome__form">
                    <div class="fr-welcome__form-block">
                        <?php if( ! empty( $location_title ) ) : ?>
                        <span>
                            <img src="<?php echo plugins_url('fencerepairs/images/location.png'); ?>" alt="Location">
                            <?php esc_html_e( $location_title ); ?>
                        </span>
                        <?php endif; ?>
                        <h2><?php esc_html_e($form_title); ?></h2>
                        <?php echo apply_filters('the_content', $content); ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php
    return ob_get_clean();
}

add_shortcode( 'code_booking_form', 'fence_booking_form_output' );
