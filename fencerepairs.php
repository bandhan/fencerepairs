<?php
/*
* Plugin Name: Fencerepair Addon
* Plugin URI: https://wpcamel.com/
* Description: A plugin that is created for generating custom sections.
* Version: 1.0.0
* Author: WPCamel
* Author URI: https://wpcamel.com/
* License: MIT License (MIT)
* Text Domain: fencerepair
*/
if (!defined('WPINC')) exit;

/**
 * Plugin assets enqueued
 *
 * @return void
 */
function fencerepair_assets() {
    wp_enqueue_style( 'jost', "//fonts.googleapis.com/css2?family=Jost:wght@500;700&display=swap" );
    wp_enqueue_style( 'poppins', "//fonts.googleapis.com/css2?family=Poppins:wght@400;500;700&display=swap" );
    wp_enqueue_style( 'slick-css', plugins_url( 'css/slick.css' , __FILE__ ) );
    wp_enqueue_style( 'addon-style', plugins_url( 'css/style.css' , __FILE__ ) );

    wp_enqueue_script( 'slick-js', plugins_url( 'js/slick.min.js' , __FILE__ ), ['jquery'], '', true );
    wp_enqueue_script( 'main', plugins_url( 'js/main.js' , __FILE__ ), ['jquery'], '', true );
}
add_action( 'wp_enqueue_scripts', 'fencerepair_assets' );

/**
 * Plugin init hook
 *
 * @return void
 */
function fencerepair_plugin_init()
{
    // Including Module files
    require_once plugin_dir_path(__FILE__) . '/modules/menu.php';
    require_once plugin_dir_path(__FILE__) . '/modules/hero-section.php';
    require_once plugin_dir_path(__FILE__) . '/modules/hero-booking-form.php';
    require_once plugin_dir_path(__FILE__) . '/modules/hire-section.php';
    require_once plugin_dir_path(__FILE__) . '/modules/service-section.php';
    require_once plugin_dir_path(__FILE__) . '/modules/portfolio-section.php';
    require_once plugin_dir_path(__FILE__) . '/modules/testimonial-section.php';
    require_once plugin_dir_path(__FILE__) . '/modules/contact-form.php';
    require_once plugin_dir_path(__FILE__) . '/modules/footer-section.php';


}
add_action('plugins_loaded', 'fencerepair_plugin_init');
